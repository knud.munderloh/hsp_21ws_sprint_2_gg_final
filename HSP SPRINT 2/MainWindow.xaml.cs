﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HSP_SPRINT_2
{
    //Eigenschaften der Mutter



    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Eingabe
        public int Mutterntyp;
        public int Gewindeart;
        public int Normmutter;
        public int Material;
        public double Schlüsselweite;
        public double Höhe;
        public double Gewindesteigung;
        public int Gewinderichtung;
        public int Stückzahl;

        //Ausgabe
        public double maxDurchmesser;
        public double Gewicht;
        public double dbl_Gewindesteigung;
        public double Volumen;
        public double dbl_Schlüsselweite;
        public double dbl_Höhe;
        public double Flankendurchmesser;
        public double Gewindetiefe;
        public double Preis;

        //Hilfsvariablen
        public double[] Dichte = new double[7] { 0, 7.85, 7.9, 8.0, 8.73, 2.7, 4.507 };   //Dichte in g/cm^3
        public double[] Materialpreis = new double[7] { 0, 0.02, 0.03, 0.04, 0.035, 0.04, 0.010 };          //Euro pro Gramm (Angaben sind ausgedacht)
        public int Fehler;




        public MainWindow()
        {
            InitializeComponent();
        }

        //Buttons

        private void Btn_Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Btn_Hilfe_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Zu Risiken und Nebenwirkungen fragen Sie bitte Ihren Arzt oder Apotheker.");
            // Funny Joke einfügen
        }

        private void Btn_Berechnung_Click(object sender, RoutedEventArgs e)
        {
            // Berechnung durchführen
            calcmaxDurchmesser();
            calcVolumen();
            calcGewicht();
            calcGewindesteigung();
            calcSchlüsselweite();
            calcHöhe();
            calcFlankendurchmesser();
            calcGewindetiefe();
            calcPreis();

            Tbl_maxDurchmesser.Text = Convert.ToString(maxDurchmesser);
            Tbl_Volumen.Text = Convert.ToString(Volumen);
            Tbl_Gewicht.Text = Convert.ToString(Gewicht);
            Tbl_Gewindesteigung.Text = Convert.ToString(Gewindesteigung);
            Tbl_Schlüsselweite.Text = Convert.ToString(dbl_Schlüsselweite);
            Tbl_Höhe.Text = Convert.ToString(dbl_Höhe);
            Tbl_Flankendurchmesser.Text = Convert.ToString(Flankendurchmesser);
            Tbl_Gewindetiefe.Text = Convert.ToString(Gewindetiefe);
            Tbl_Preis.Text = Convert.ToString(Preis);

            Grd_Ausgabe.Visibility = Visibility.Visible;
        }

        private void Btn_InDenWarenkorb_Click(object sender, RoutedEventArgs e)
        {
            // Erstellte Mutter in den Warenkorb übergeben (nur eigegebene Daten)
        }

        private void Tvi_Sechskantmutter_Selected(object sender, RoutedEventArgs e)
        {
            Rb_Normmutter.Visibility = Visibility.Visible;
            Rb_Spezifizierbar.Visibility = Visibility.Visible;
            Rb_Spezialanfertigung.Visibility = Visibility.Visible;

            Mutterntyp = 0;
        }
        private void Tvi_Vierkantmutter_Selected(object sender, RoutedEventArgs e)
        {
            Rb_Normmutter.Visibility = Visibility.Visible;
            Rb_Spezifizierbar.Visibility = Visibility.Visible;
            Rb_Spezialanfertigung.Visibility = Visibility.Visible;

            Mutterntyp = 1;
        }
        private void Tvi_Flanschmutter_Selected(object sender, RoutedEventArgs e)
        {
            Rb_Normmutter.Visibility = Visibility.Visible;
            Rb_Spezifizierbar.Visibility = Visibility.Visible;
            Rb_Spezialanfertigung.Visibility = Visibility.Visible;

            Mutterntyp = 2;
        }
        private void Tvi_Hutmutter_Selected(object sender, RoutedEventArgs e)
        {
            Rb_Normmutter.Visibility = Visibility.Visible;
            Rb_Spezifizierbar.Visibility = Visibility.Visible;
            Rb_Spezialanfertigung.Visibility = Visibility.Visible;

            Mutterntyp = 3;
        }

        private void Rb_Normmutter_Checked(object sender, RoutedEventArgs e)
        {
            Normmutter = 0;

            Lbl_Nenndurchmesser.Visibility = Visibility.Visible;
            Cb_Nenndurchmesser.Visibility = Visibility.Visible;
            Lbl_Material.Visibility = Visibility.Visible;
            Cb_Material.Visibility = Visibility.Visible;
            Lbl_Stückzahl.Visibility = Visibility.Visible;
            Tb_Stückzahl.Visibility = Visibility.Visible;

            Lbl_Gewindeart.Visibility = Visibility.Collapsed;
            Lbl_Gewindesteigung.Visibility = Visibility.Collapsed;
            Lbl_Höhe.Visibility = Visibility.Collapsed;
            Lbl_Schlüsselweite.Visibility = Visibility.Collapsed;
            Lbl_Gewinderichtung.Visibility = Visibility.Collapsed;
            Cb_Gewinderichtung.Visibility = Visibility.Collapsed;

            Cb__Gewindeart.Visibility = Visibility.Collapsed;
            Tb_Schlüsselweite.Visibility = Visibility.Collapsed;
            Tb_Höhe.Visibility = Visibility.Collapsed;
            Cb_Gewindesteigung.Visibility = Visibility.Collapsed;
        }

        private void Spezifizierbar_Checked(object sender, RoutedEventArgs e)
        {
            Normmutter = 1;

            Lbl_Nenndurchmesser.Visibility = Visibility.Visible;
            Cb_Nenndurchmesser.Visibility = Visibility.Visible;
            Lbl_Material.Visibility = Visibility.Visible;
            Cb_Material.Visibility = Visibility.Visible;
            Lbl_Stückzahl.Visibility = Visibility.Visible;
            Tb_Stückzahl.Visibility = Visibility.Visible;

            Lbl_Gewindeart.Visibility = Visibility.Visible;
            Cb__Gewindeart.Visibility = Visibility.Visible;
            Lbl_Gewindesteigung.Visibility = Visibility.Visible;
            Cb_Gewindesteigung.Visibility = Visibility.Visible;
            Lbl_Gewinderichtung.Visibility = Visibility.Visible;
            Cb_Gewinderichtung.Visibility = Visibility.Visible;

            Lbl_Höhe.Visibility = Visibility.Collapsed;
            Lbl_Schlüsselweite.Visibility = Visibility.Collapsed;
            Tb_Schlüsselweite.Visibility = Visibility.Collapsed;
            Tb_Höhe.Visibility = Visibility.Collapsed;

        }

        private void Spezialanfertigung_Checked(object sender, RoutedEventArgs e)
        {
            Normmutter = 2;

            Lbl_Nenndurchmesser.Visibility = Visibility.Visible;
            Cb_Nenndurchmesser.Visibility = Visibility.Visible;
            Lbl_Material.Visibility = Visibility.Visible;
            Cb_Material.Visibility = Visibility.Visible;
            Lbl_Stückzahl.Visibility = Visibility.Visible;
            Tb_Stückzahl.Visibility = Visibility.Visible;

            Lbl_Gewindeart.Visibility = Visibility.Visible;
            Cb__Gewindeart.Visibility = Visibility.Visible;
            Lbl_Gewindesteigung.Visibility = Visibility.Visible;
            Cb_Gewindesteigung.Visibility = Visibility.Visible;
            Lbl_Gewinderichtung.Visibility = Visibility.Visible;
            Cb_Gewinderichtung.Visibility = Visibility.Visible;

            Lbl_Höhe.Visibility = Visibility.Visible;
            Lbl_Schlüsselweite.Visibility = Visibility.Visible;
            Tb_Schlüsselweite.Visibility = Visibility.Visible;
            Tb_Höhe.Visibility = Visibility.Visible;

        }

        // Ab Hier beginnt die Farbliche Eingabenüberprüfung

        private void Tb_Schlüsselweite_LostFocus(object sender, RoutedEventArgs e) // text changed ändern!!!!
        {
            TextBox tb = (TextBox)sender;
            double res;

            if (Double.TryParse(tb.Text, out res))
            {
                //tb.Background = Brushes.LightGreen;
                tb.BorderBrush = Brushes.Green;
            }
            else
            {
                //tb.Background = Brushes.LightPink;
                tb.BorderBrush = Brushes.Red;
                //tb.Text.setFocus = true; findet mecke vllt raus
            }
        }

        private void Tb_Höhe_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            double res;

            if (Double.TryParse(tb.Text, out res))
            {
                tb.Background = Brushes.LightGreen;
            }
            else
            {
                tb.Background = Brushes.LightPink;
            }
        }
        private void Tb_Stückzahl_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            double res;

            if (Double.TryParse(tb.Text, out res))
            {
                tb.Background = Brushes.LightGreen;
            }
            else
            {
                tb.Background = Brushes.LightPink;
            }
        }

        // Hier ended die Eingabenüberprüfung

        //Hier beginnt die Zuweisung der Gewindeart

        private void Cbi__Regelgewinde_Selected(object sender, RoutedEventArgs e)
        {
            Gewindeart = 0;
        }

        private void Cbi__Feingewinde_Selected(object sender, RoutedEventArgs e)
        {
            Gewindeart = 1;
        }

        private void Cbi__Trapezgewinde_Selected(object sender, RoutedEventArgs e)
        {
            Gewindeart = 2;
        }

        //Zuweisung Material
        private void Cbi_Stahl_Selected(object sender, RoutedEventArgs e)
        {
            Material = 0;
        }

        private void Cbi_V2A_Selected(object sender, RoutedEventArgs e)
        {
            Material = 1;
        }

        private void Cbi_V4A_Selected(object sender, RoutedEventArgs e)
        {
            Material = 2;
        }

        private void Cbi_Messing_Selected(object sender, RoutedEventArgs e)
        {
            Material = 3;
        }

        private void Cbi_Aluminium_Selected(object sender, RoutedEventArgs e)
        {
            Material = 4;
        }

        private void Cbi_Titan_Selected(object sender, RoutedEventArgs e)
        {
            Material = 5;
        }

        //Zuweisung Gewinderichtung

        private void Cbi_Rechtsdrehend_Selected(object sender, RoutedEventArgs e)
        {
            Gewinderichtung = 0;
        }

        private void Cbi_Linksdrehend_Selected(object sender, RoutedEventArgs e)
        {
            Gewinderichtung = 1;
        }

        //Ausgabe Volumen

        public void calcmaxDurchmesser()

        {
            maxDurchmesser = 10; //Wert für Testrechnungen

            if (Mutterntyp == 0 || Mutterntyp == 3)
            {
                switch (Normmutter)
                {
                    case 0:
                        //Sechskant/Hut Normmutter
                        //maxDurchmesser =
                        break;

                    case 1:
                        //Sechskant/hut Spezifizierbar
                        //maxDurchmesser =
                        break;
                    case 2:
                        //Sechskant/hut Spezialafertigung
                        //maxDurchmesser =
                        break;
                }
            }
            else if (Mutterntyp == 1)
            {
                switch (Normmutter)
                {
                    case 0:
                        //Vierkant Normmutter
                        //maxDurchmesser =
                        break;

                    case 1:
                        //Vierkant Spezifizierbar
                        //maxDurchmesser =
                        break;
                    case 2:
                        //Vierkant Spezialafertigung
                        //maxDurchmesser =
                        break;
                }
            }
            else if (Mutterntyp == 2)
            {
                switch (Normmutter)
                {
                    case 0:
                        //Flansch Normmutter
                        //maxDurchmesser =
                        break;

                    case 1:
                        //Flansch Spezifizierbar
                        //maxDurchmesser =
                        break;
                    case 2:
                        //Flansch Spezialafertigung
                        //maxDurchmesser =
                        break;
                }
            }
            else
            {
                MessageBox.Show("Kritischer Fehler! Bitte kontaktieren Sie den Support!");
                Application.Current.Shutdown();
            }
        }

        public void calcVolumen()
        {
            Volumen = 10;
        }

        public void calcGewicht()
        {
            Gewicht = 10;
        }

        public void calcGewindesteigung()
        {
            Gewindesteigung = 10;
        }

        public void calcSchlüsselweite()
        {
            Schlüsselweite = 10;
        }

        public void calcHöhe()
        {
            Höhe = 10;
        }

        public void calcFlankendurchmesser()
        {
            Flankendurchmesser = 10;
        }

        public void calcGewindetiefe()
        {
            Gewindetiefe = 10;
        }

        public void calcPreis()
        {
            Preis = 10;
        }
        //ENDE der Berechnungen

        //Start Eingabemethoden

        private void Tb_Schlüsselweite_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            double res = 0;
            if (double.TryParse(tb.Text, out res))
            {

            }
        }

        //WICHTIG ZU PRÜFEN
        public void cbi_Selected(object sender, RoutedEventArgs e)
        {
            ComboBoxItem cbi = (ComboBoxItem)sender;
            double result = Convert.ToDouble(Tag);
        }
    }
}

